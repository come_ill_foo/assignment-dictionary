SRCDIR=src
INCDIR=inc
BUILDDIR=build
APPNAME=dict

ASM=nasm
ASMFLAGS=-f elf64 -i $(INCDIR)/ -g
LD=ld

all: $(APPNAME)

build:
	mkdir $(BUILDDIR)

$(APPNAME): $(BUILDDIR)/main.o $(BUILDDIR)/dict.o $(BUILDDIR)/lib.o
	$(LD) -o $@ $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.asm build
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean

clean:
	rm -rf $(BUILDDIR)
	rm -f $(APPNAME)