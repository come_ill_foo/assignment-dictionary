section .text
global find_word
extern string_equals
%include "lib.inc"

; find_word пройдёт по всему словарю
; в поисках подходящего ключа. Если
; подходящее вхождение найдено, вернёт
; адрес начала вхождения в словарь
; (не значения), иначе вернёт 0.
; Функция принимает:
; + Указатель на нуль-терминированную строку. rdi
; + Указатель на начало словаря. rsi
find_word:
.loop:
    test rsi, rsi
    jz .return_error
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jz .goto_next
    mov rax, rsi
    ret
.goto_next:
    mov rsi, [rsi]
    jmp .loop
.return_error:
    xor rax, rax
    ret