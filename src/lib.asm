section .text
global exit
global string_length
global print_string
global eprint_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax
    mov rax, 60 ; 'exit' syscall number
    xor rdi, rdi ; the return code
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push rbx
    xor rax, rax
.loop_enter:
    xor rbx, rbx
    add bl, [rdi]
    jz .loop_exit
    inc rax
    inc rdi
    jmp .loop_enter
.loop_exit:
    pop rbx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi ; string address
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ; string length
    mov rdi, 1   ; stdout descriptor
    mov rax, 1   ; 'write' syscall number
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
eprint_string:
    xor rax, rax
    mov rsi, rdi ; string address
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ; string length
    mov rdi, 2   ; stderr descriptor
    mov rax, 1   ; 'write' syscall number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi      ; to save char in memory
    mov rsi, rsp  ; string address copy
    mov rdx, 1    ; string length
    mov rdi, 1    ; stdout descriptor
    mov rax, 1    ; 'write' syscall number
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA  ; \n - newline symbol
    call print_char
    ret


__print_uint:
    xor rax, rax
    xor rdx, rdx ; to not get sigfpe
    add rax, rdi
    jz .total_end
.begin:
    div rsi
    jz .end
    push rdx
    mov rdi, rax
    call __print_uint
    pop rdx
.end:
    mov rdi, rdx
    add rdi, 0x30
    call print_char
.total_end:
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; suppose rdi - is an argument
; suppose rax - is a divisor
; suppose rdx - is a remainder
; let rsi is a dividend
print_uint:
    mov rsi, 10
    xor rax, rax
    xor rdx, rdx ; to not get sigfpe
    mov rax, rdi
    div rsi
    or rax, rdx
    jz .just_print
    call __print_uint
    jmp .end
.just_print:
    mov rdi, rdx
    add rdi, 0x30
    call print_char
.end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    add rax, rdi
    jge .uprint
    push rdi
    mov rdi, 0x2d
    call print_char
    pop rdi
    neg rdi
.uprint:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; suppose rdi is a first string pointer
; suppose rsi is a second string pointer
string_equals:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    ; check the strings' lengths
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    xchg rdi, rsi
    push rdi
    call string_length
    pop rdi
    xchg rdi, rsi
    cmp rax, rdx
    jnz .not_equal
.start_cmp:
    xor rax, rax
    xor rcx, rcx
    cmp rdx, 0
    jz .equal
    dec rdx
    mov al, [rdi]
    mov cl, [rsi]
    cmp al, cl
    jnz .not_equal
    inc rdi
    inc rsi
    jmp .start_cmp
.equal:
    mov rax, 1
    ret
.not_equal:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0x0
    mov rdx, 1     ; char count
    mov rdi, 0     ; stdin descriptor
    mov rax, 0     ; read syscall number
    mov rsi, rsp   ; destination address
    syscall
    pop rsi        ; the char value
    or rax, rax    ; check if zero bytes read
    jz .err_return
    cmp rax, -1   ; check for errors
    jz .err_return
    xor rax, rax
    add rax, rsi
    ret
.err_return:
    xor rax, rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; suppose rdi is a buffer address
; suppose rsi is a buffer length

read_word:
    xor rdx, rdx
    xor rax, rax
.body:
    dec rsi
    jz .maybe_success
    jl .failure
.reading:
    ; read symbol start
    push rdi
    push rsi
    push rdx
    call read_char    ; now we have char code in rax or 0 if error occurs
    pop rdx
    pop rsi
    pop rdi
    ; read symbol end
    jz .end_of_stream ; if the eof occurs
    ; is whitespace start
    cmp rax, 0x20     ; check if space
    jz .maybe_success
    cmp rax, 0x9      ; check if tab
    jz .maybe_success
    cmp rax, 0xA      ; check if newline
    jz .maybe_success
    ; is whitespace end
    ; copy to memory start
    push rsi
    push rax       ; store in stack
    mov rcx, 1
    mov rsi, rsp
    cld
    rep movsb
    pop rax
    pop rsi
    ; copy to memory end
    inc rdx
    jmp .body
.maybe_success:
    cmp rdx, 0
    jz .reading
.end_of_stream:
    ; 0 copy to memory start
    push rsi
    push 0       ; store in stack
    mov rcx, 1
    mov rsi, rsp
    cld
    rep movsb
    dec rdi
    pop rax
    pop rsi
    ; 0 copy to memory end
    sub rdi, rdx
    mov rax, rdi
    ret
.failure:
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; suppose rdi is the pointer to the string
parse_uint:
    mov rsi, 10
    xor rax, rax
    xor rdx, rdx
.body:
    xor rcx, rcx
    add cl, [rdi]
    jz .end       ; if null terminator encountered
    sub cl, 0x30 ; substract zero ASCII code
    cmp cl, 9
    jg .error_ret
    cmp cl, 0
    jl .error_ret
    push rdx
    mul rsi
    pop rdx
    add rax, rcx
    inc rdx
    inc rdi
    jmp .body
.error_ret:
    cmp rdx, 0
    jnz .end
    xor rdx, rdx
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    add al, [rdi]
    cmp al, 0x2d ; check if dash as minus
    jz .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; suppose rdi is the source pointer
; suppose rsi is the destination pointer
; suppose rdx is the length of the buffer
string_copy:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    inc rax
    cmp rax, rdx
    jle .enough_space
    xor rax, rax
    jmp .end
.enough_space:
; copy logic
; rep movsb copy rcx bytes from rsi to rdi
    xchg rsi, rdi
    mov rcx, rax
    cld
    rep movsb
.end:
    ret
