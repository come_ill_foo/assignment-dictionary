%define buffer_length 256

%macro strstkstore 1-*
    %assign argc %0
    %assign argc argc - 1
    %rep argc
        sub rsp, 1
        mov [rsp], byte %{-1:-1}
        %rotate -1
    %endrep
    %strlen length %{-1:-1}
    %assign i length
    %rep length
        %substr char %{-1:-1} i
        %assign i i - 1 
        sub rsp, 1
        mov [rsp], byte char
    %endrep
%endmacro

%include "words.inc"
section .text
global _start
extern read_word
extern exit
extern find_word
extern print_string
extern print_newline
extern eprint_string
extern string_length

_start:
    ; read the word from stdin
    sub rsp, buffer_length
    mov rdi, rsp
    mov rsi, buffer_length
    push rsi
    push rdi
    call read_word
    pop rdi
    ; find the word
    mov rsi, next_label
    call find_word
    pop rsi
    ; check if not exists
    test rax, rax
    jz .error
    ; print if exists
    push rdi
    lea rdi, [rax + 8]
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    pop rdi
    call print_newline
    jmp .exit
.error:
    strstkstore "err: the word doesn't exist", 0xa, 0x0
    mov rdi, rsp
    call eprint_string
    add rsp, 28
    mov rdi, 1
.exit:
    call exit
