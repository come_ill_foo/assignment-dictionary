%define next_label 0
%macro colon 2
%%some_label: dq next_label
db %1, 0
%2:
%define next_label %%some_label
%endmacro